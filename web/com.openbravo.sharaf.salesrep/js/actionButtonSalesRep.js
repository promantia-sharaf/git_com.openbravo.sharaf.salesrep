/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone */

enyo.kind({
  name: 'OB.OBPOSPointOfSale.UI.EditLine.SharafSalesRepButton',
  kind: 'OB.UI.SmallButton',
  classes: 'btnlink-orange',
  handlers: {
	    onSetMultiSelected: 'setMultiSelected'
	  },
  tap: function () {
    var me = this;
    OB.MobileApp.view.waterfall('onShowPopup', {
      popup: 'SHASR_UI_SalesRepresentativePopup',
      args: {
        selectedModels: me.owner.owner.selectedModels
      }
    });
  },
  
  setMultiSelected: function (inSender, inEvent) {
	    var isNegativeLine = false, returnCount=0;    
	    inEvent.models.forEach(function (p){
	    	if (p.get('originalDocumentNo') && returnCount == 0) {
	    		isNegativeLine = true;
	    		returnCount++;
      	  	}
	    });
	    
	    if (isNegativeLine) {
	    	this.setShowing(false);
        } else {
        	this.setShowing(true);
        }
},
  
  init: function (model) {
    this.model = model;
    this.model.get('order').on('change:isPaid change:isEditable', function (newValue) {
      if (newValue) {
        if (newValue.get('isPaid') === true || newValue.get('isEditable') === false) {
          this.setShowing(false);
          return;
        }
      }
      this.setShowing(true);
    }, this);
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('SHASR_BtnSalesRepresentative'));
  }
});

OB.OBPOSPointOfSale.UI.EditLine.prototype.actionButtons.push({
  kind: 'OB.OBPOSPointOfSale.UI.EditLine.SharafSalesRepButton',
  name: 'sharafSalesRepButton'
});