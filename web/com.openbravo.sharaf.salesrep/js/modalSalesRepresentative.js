/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

/*scrollable table (body of modal)*/
enyo.kind({
  name: 'SHASR.UI.ListSrs',
  classes: 'row-fluid',
  handlers: {
    onSearchAction: 'searchAction',
    onClearAction: 'clearAction'
  },
  events: {
    onChangeSalesRepresentative: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'srslistitemprinter',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '400px',
          renderHeader: 'OB.UI.ModalSrScrollableHeader',
          renderLine: 'OB.UI.ListSrsLine',
          renderEmpty: 'OB.UI.RenderEmpty'
        }]
      }]
    }]
  }],
  clearAction: function (inSender, inEvent) {
    this.srsList.reset();
    return true;
  },
  searchAction: function (inSender, inEvent) {
    var me = this,
        filter = inEvent.srName;

    function errorCallback(tx, error) {
      OB.UTIL.showError("OBDAL error: " + error);
    }

    function successCallbackBPs(dataSrs) {
      if (dataSrs && dataSrs.length > 0) {
        dataSrs.unshift({
          id: null,
          _identifier: null,
          name: OB.I18N.getLabel('OBPOS_None')
        });
        me.srsList.reset(dataSrs.models);
      } else {
        me.srsList.reset();
      }
    }

    var criteria = {};
    if (filter && filter !== '') {
      criteria._identifier = {
        operator: OB.Dal.CONTAINS,
        value: filter
      };
    }

//    if (OB.MobileApp.model.hasPermission('OBPOS_remote.customer', true)) {
//      var filterIdentifier = {
//        columns: ['_filter'],
//        operator: 'startsWith',
//        value: filter
//      };
//      var remoteCriteria = [filterIdentifier];
//      criteria.remoteFilters = remoteCriteria;
//    }

    OB.Dal.find(OB.Model.SalesRepresentative, criteria, successCallbackBPs, errorCallback);
    return true;
  },
  srsList: null,
  init: function (model) {
    this.srsList = new Backbone.Collection();
    this.$.srslistitemprinter.setCollection(this.srsList);
    this.srsList.on('click', function (model) {
      if (model.attributes.name === OB.I18N.getLabel('OBPOS_None')) {
        model.attributes.name = null;
      }
      this.owner.owner.args.callback(model);
    }, this);
  }
});

/*Modal definiton*/
enyo.kind({
  name: 'SHASR.UI.ModalSalesRepresentative',
  topPosition: '125px',
  kind: 'OB.UI.Modal',
  executeOnHide: function () {
    this.$.body.$.listSrs.$.srslistitemprinter.$.theader.$.modalSrScrollableHeader.clearAction();
  },
  i18nHeader: 'OBPOS_LblAssignSalesRepresentative',
  body: {
    kind: 'SHASR.UI.ListSrs'
  },
  init: function (model) {
    this.model = model;
    this.waterfall('onSetModel', {
      model: this.model
    });
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'SHASR.UI.ModalSalesRepresentative',
  name: 'SHASR_UI_ModalSalesRepresentative'
});