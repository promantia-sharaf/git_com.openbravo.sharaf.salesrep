/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'SHASR.UI.SalesRepresentativePopup',
  kind: 'OB.UI.ModalAction',
  i18nHeader: 'OBPOS_SalesRepresentative',
  handlers: {
    onApplyChanges: 'applyChanges',
    onSetSalesRep: 'setSalesRep',
    onSetValue: 'setValue'
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'SHASR.UI.SalesRepApply'
    }, {
      kind: 'OB.UI.CancelDialogButton'
    }]
  },
  executeOnShow: function () {
    this.waterfall('onLoadValue', {
      selectedModels: this.args.selectedModels
    });
    this.changedSalesRep = false;
    this.shasrSalesrep = null;
    this.shasrSalesrepName = null;
    this.shasrSalesrepUserName = null;
  },
  setSalesRep: function (inSender, inEvent) {
    this.changedSalesRep = true;
    this.shasrSalesrep = inEvent.id;
    this.shasrSalesrepName = inEvent.name;
    this.shasrSalesrepUserName = inEvent.username;
  },
  applyChanges: function (inSender, inEvent) {
    if (OB.MobileApp.model.hasPermission('OBPOS_SR.comboOrModal', true)) {
      if (this.changedSalesRep) {
        var i;
        for (i = 0; i < this.args.selectedModels.length; i++) {
          this.args.selectedModels[i].set('shasrSalesrep', this.shasrSalesrep);
          this.args.selectedModels[i].set('shasrSalesrepName', this.shasrSalesrepName);
          this.args.selectedModels[i].set('shasrSalesrepUserName', this.shasrSalesrepUserName);
        }
      }
    } else {
      this.waterfall('onApplyChange', {});
    }
    return true;
  },
  setValue: function (inSender, inEvent) {
    var i;
    for (i = 0; i < this.args.selectedModels.length; i++) {
      this.args.selectedModels[i].set(inEvent.modelProperty, inEvent.value);
    }
    return true;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'SHASR.UI.SalesRepresentativePopupImpl',
  kind: 'SHASR.UI.SalesRepresentativePopup',
  newAttributes: [{
    kind: 'SHASR.UI.SalesRepBox',
    name: 'salesRepBox',
    i18nLabel: 'OBPOS_SalesRepresentative'
  }, {
    kind: 'SHASR.UI.SalesRepresentative',
    name: 'salesrepresentativebutton',
    i18nLabel: 'OBPOS_SalesRepresentative',
    permission: 'OBPOS_salesRepresentative.receipt',
    permissionOption: 'OBPOS_SR.comboOrModal'
  }]
});

enyo.kind({
  name: 'SHASR.UI.SalesRepBox',
  kind: 'OB.UI.renderComboProperty',
  events: {
    onSetValue: ''
  },
  modelProperty: 'shasrSalesrep',
  permission: 'OBPOS_salesRepresentative.receipt',
  permissionOption: 'OBPOS_SR.comboOrModal',
  retrievedPropertyForValue: 'id',
  retrievedPropertyForText: '_identifier',
  init: function (model) {
    this.collection = new OB.Collection.SalesRepresentativeList();
    this.model = model;
    this.doLoadValueNeeded = true;
    if (!OB.MobileApp.model.hasPermission(this.permission)) {
      this.doLoadValueNeeded = false;
      this.parent.parent.parent.hide();
    } else {
      if (OB.MobileApp.model.hasPermission(this.permissionOption, true)) {
        this.doLoadValueNeeded = false;
        this.parent.parent.parent.hide();
      }
    }
  },

  // override to not load things upfront when not needed
  loadValue: function (inSender, inEvent) {
    if (this.doLoadValueNeeded) {
      inEvent.modelProperty = this.modelProperty;
      inEvent.model = inEvent.selectedModels && inEvent.selectedModels.length > 0 ? inEvent.selectedModels[0] : null;
      // call the super implementation in the prototype directly
      OB.UI.renderComboProperty.prototype.loadValue.apply(this, arguments);
    }
  },

  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      this.doSetValue({
        modelProperty: this.modelProperty,
        value: selected.get(this.retrievedPropertyForValue)
      });
    }
  },

  fetchDataFunction: function (args) {
    var me = this,
        actualUser;

    OB.Dal.find(OB.Model.SalesRepresentative, null, function (data) {
      if (me.destroyed) {
        return;
      }
      if (data.length > 0) {
        data.unshift({
          id: null,
          _identifier: null
        });
        me.dataReadyFunction(data, args);
        var i, index = 0;
        if (args.model && args.model.get('shasrSalesrep')) {
          for (i = 0; i < me.collection.models.length; i++) {
            if (me.collection.models[i].get('id') === args.model.get('shasrSalesrep')) {
              index = i;
              break;
            }
          }
        }
        me.$.renderCombo.setSelected(index);
      } else {
        actualUser = new OB.Model.SalesRepresentative();
        actualUser.set('_identifier', me.model.get('order').get('salesRepresentative$_identifier'));
        actualUser.set('id', me.model.get('order').get('salesRepresentative'));
        data.models = [actualUser];
        me.dataReadyFunction(data, args);
      }

    }, function () {
      OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingSalesRepresentative'));
      me.dataReadyFunction(null, args);
    }, args);
  }
});

enyo.kind({
  kind: 'OB.UI.SmallButton',
  name: 'SHASR.UI.SalesRepresentative',
  classes: 'btnlink btnlink-small btnlink-gray',
  style: 'float:left; margin:7px; height:27px; padding: 4px 15px 7px 15px;',
  events: {
    onShowPopup: ''
  },
  handlers: {
    onLoadValue: 'loadValue'
  },
  tap: function () {
    if (!this.disabled) {
      var me = this;
      this.doShowPopup({
        popup: 'SHASR_UI_ModalSalesRepresentative',
        args: {
          callback: function (result) {
            me.renderSalesRepresentative(result.get('name'));
            me.bubble('onSetSalesRep', {
              id: result.get('id'),
              name: result.get('name'),
              username: result.get('username')
            });
          }
        }
      });
    }
  },
  loadValue: function (inSender, inEvent) {
    var name = '',
        numDifferentSalesRep;
    var isQuotaiontoInvoice = false;
    if (inEvent.selectedModels && inEvent.selectedModels.length === 1) {
      name = inEvent.selectedModels[0].get('shasrSalesrepName');
      if(!OB.UTIL.isNullOrUndefined(inEvent.selectedModels[0].get('shaquoOriginalDocumentNo')) && inEvent.selectedModels[0].get('shaquoOriginalDocumentNo').includes('SO')){
    	  isQuotaiontoInvoice = true;
    	  }
    } else if (inEvent.selectedModels && inEvent.selectedModels.length > 1) {
      numDifferentSalesRep = _.toArray(_.groupBy(inEvent.selectedModels, function (line) {
        return line.get('shasrSalesrep');
      })).length;
      if (numDifferentSalesRep === 1) {
        name = inEvent.selectedModels[0].get('shasrSalesrepName');
        if(!OB.UTIL.isNullOrUndefined(inEvent.selectedModels[0].get('shaquoOriginalDocumentNo')) && inEvent.selectedModels[0].get('shaquoOriginalDocumentNo').includes('SO')){
        	isQuotaiontoInvoice = true;
        	}
      } else {
        name = '';
        if(!OB.UTIL.isNullOrUndefined(inEvent.selectedModels[0].get('shaquoOriginalDocumentNo')) && inEvent.selectedModels[0].get('shaquoOriginalDocumentNo').includes('SO')){
        	isQuotaiontoInvoice = true;
        	}
      }
    }
    this.renderSalesRepresentative(name,isQuotaiontoInvoice);
  },
  init: function (model) {
    if (!OB.MobileApp.model.hasPermission(this.permission)) {
      this.parent.parent.parent.hide();
    } else {
      if (!OB.MobileApp.model.hasPermission(this.permissionOption, true)) {
        this.parent.parent.parent.hide();
      }
    }
  },
  renderSalesRepresentative: function (newSalesRepresentative,isQuotaiontoInvoice) {
    this.setContent(newSalesRepresentative);
    if(isQuotaiontoInvoice){
    	this.setDisabled();
    }else{
    	this.setDisabled(false);	
    }
  }
});

enyo.kind({
  name: 'SHASR.UI.SalesRepApply',
  kind: 'OB.UI.ModalDialogButton',
  events: {
    onHideThisPopup: '',
    onApplyChanges: ''
  },
  tap: function () {
    this.doApplyChanges();
    this.doHideThisPopup();
    this.model.get('order').save();
    this.model.get('orderList').saveCurrent();
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBMOBC_LblApply'));
  },
  init: function (model) {
    this.model = model;
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'SHASR.UI.SalesRepresentativePopupImpl',
  name: 'SHASR_UI_SalesRepresentativePopup'
});