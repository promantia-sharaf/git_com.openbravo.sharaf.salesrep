/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreOrderSave', function (args, callbacks) {
    var lines = args.receipt.get('lines'),
        receipt = args.receipt,
        isError = false,
        isOldCustomerError = false,
        isLatitudeLongitudeError = false,
        latitudeLongitudeErrorMessage ='',
        dtKey = receipt.get('custsdtDocumenttypeSearchKey'),
        endCustomer = false;

    if (args.cancellation) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }


    var sharafDocArr = OB.MobileApp.model.get('sharafDocumentType');
    for (i = 0; i < sharafDocArr.length; i++) {
      if (sharafDocArr[i].searchKey === dtKey && sharafDocArr[i].shfptEndCustomer) {
        endCustomer = true;
      }
    }

    if (args.receipt.get('isQuotation')) {
      _.each(lines.models, function (line) {
        if (OB.UTIL.isNullOrUndefined(line.get('shasrSalesrep'))) {
          isError = true;
        }
      });
      // OBDEV - 273
       var cityName = OB.MobileApp.model.receipt.attributes.bp.attributes.cityName;
       var countryId = OB.MobileApp.model.receipt.attributes.bp.attributes.locationModel.attributes.countryId;
       
           if(OB.UTIL.isNullOrUndefined(cityName) || cityName == '' ) {
            isOldCustomerError = true;
           } else {
                    var query = "select p.id from custsha_city p where p.name = '" + cityName + "'"
                          +" and p.country = '" + countryId + "'" ;
                    OB.Dal.queryUsingCache(OB.Model.City, query, [], function(res) {
                       if(!res.models.length > 0){
                          isOldCustomerError = true; 
                        }                                             
                     });
          }
     // OBDEV-273 for Latitude and longitude
    
        for (i = 0; i < lines.length; i++) {
            for (j = i; j < lines.length; j++) {
               if (OB.MobileApp.model.get('permissions').CUSTSHA_Alloweddeliverytype_googlemap && 
                     OB.MobileApp.model.get('permissions').CUSTSHA_Alloweddeliverytype_googlemap.includes(lines.models[i].get('cUSTDELDeliveryCondition'))) {
                   if (OB.UTIL.isNullOrUndefined(receipt.get('custshaLatitude')) || OB.UTIL.isNullOrUndefined(receipt.get('custshaLongitude'))) {
                          if (!OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationModel')) && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationModel').get('custshaLongitude'))
                                && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationModel').get('custshaLatitude')) && !endCustomer) {
                              receipt.set('custshaLatitude', +receipt.get('bp').get('locationModel').get('custshaLatitude'));
                              receipt.set('custshaLongitude', +receipt.get('bp').get('locationModel').get('custshaLongitude'));
                          } else if(!OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationBillModel')) && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationBillModel').custshaLongitude)
                                && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationBillModel').custshaLatitude) && !endCustomer) {
                              receipt.set('custshaLatitude', +receipt.get('bp').get('locationBillModel').custshaLatitude);
                              receipt.set('custshaLongitude', +receipt.get('bp').get('locationBillModel').custshaLongitude);
                          } else if (!OB.UTIL.isNullOrUndefined(receipt.get('bp').get('custshaLatitude')) && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('custshaLongitude')) && !endCustomer) {
                              receipt.set('custshaLatitude', +receipt.get('bp').get('custshaLatitude'));
                              receipt.set('custshaLongitude', +receipt.get('bp').get('custshaLongitude'));
                          }else {
                              args.cancellation = true;
                              if (OB.MobileApp.model.get('sharafDeliveryConditions') && 
                                  OB.MobileApp.model.get('sharafDeliveryConditions').find(o => o.searchKey == lines.models[i].get('cUSTDELDeliveryCondition'))) {
                                  isLatitudeLongitudeError = true;
                                  latitudeLongitudeErrorMessage = 'Mandatory to capture the latitude and longitude coordinates for ' +  OB.MobileApp.model.get('sharafDeliveryConditions').find(o => o.searchKey == lines.models[i].get('cUSTDELDeliveryCondition')).name
                                     + ' items. Please select these coordinates in the customer information section.';
                              } else {
                                   isLatitudeLongitudeError = true; 
                                   latitudeLongitudeErrorMessage = 'Mandatory to capture the latitude and longitude coordinates.'                           
                                      + ' Please select these coordinates in the customer information section.'
                              }
                          }
                   }
               }
            }
          }
   // End of OBDEV-273
  }
   
    
   
    if (isError) {
      receipt.set('isEditable', true);
      args.cancellation = true;
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('SHASR_SelectSalesRep'), OB.I18N.getLabel('SHASR_SelectSalesRep'), [{
        label: OB.I18N.getLabel('OBMOBC_LblOk'),
        isConfirmButton: true,
        action: function () {
          return;
        }
      }]);
    } else if (isOldCustomerError) {
      receipt.set('isEditable', true);
      args.cancellation = true;
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      OB.UTIL.showConfirmation.display('City Name is Mandatory in Customer Address, Please Update Customer Address', [{ 
        label: OB.I18N.getLabel('OBMOBC_LblOk'),
        isConfirmButton: true,
        action: function () {
          return;
        }
      }]);
    }else if(isLatitudeLongitudeError){
      receipt.set('isEditable', true);
      args.cancellation = true;
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      OB.UTIL.showConfirmation.display(latitudeLongitudeErrorMessage, [{
        label: OB.I18N.getLabel('OBMOBC_LblOk'),
        isConfirmButton: true,
        action: function () {
          return;
        }
      }]);
    }else {
      _.each(lines.models, function (line) {
        if (OB.UTIL.isNullOrUndefined(line.get('shasrSalesrep'))) {
          line.set('shasrSalesrep', receipt.get('salesRepresentative'));
        }
      });
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());
