/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.salesrep;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

@Qualifier(PaidReceipts.paidReceiptsLinesPropertyExtension)
public class PaidReceiptSalesrepProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    List<HQLProperty> props = new ArrayList<HQLProperty>();
    props.add(new HQLProperty("coalesce(ordLine.shasrSalesrep.id, ordLine.shasrSalesrep.id, null)",
        "shasrSalesrep"));
    props.add(new HQLProperty(
        "coalesce((select usr.name from ADUser usr where usr = ordLine.shasrSalesrep), null)",
        "shasrSalesrepName"));
    props.add(new HQLProperty(
        "coalesce((select usr.username from ADUser usr where usr = ordLine.shasrSalesrep), null)",
        "shasrSalesrepUserName"));
    return props;
  }
}
